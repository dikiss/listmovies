import 'package:flutter/material.dart';
import 'package:flutter_list_movie/pages/widgets/loading/loading.dart';
import 'package:flutter_list_movie/pages/widgets/movies/movie_list.dart';
import 'package:flutter_list_movie/pages/widgets/placeholder/placeholder.dart';
import 'package:flutter_list_movie/redux/app/app_state.dart';
import 'package:flutter_list_movie/redux/movies/movies_list_actions.dart';
import 'package:flutter_list_movie/viewmodels/movie_list_view_model.dart';
import 'package:flutter_redux/flutter_redux.dart';

class MovieListPage extends StatelessWidget {
  const MovieListPage();

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, MovieListPageViewModel>(
      distinct: true,
      onInit: (store) => store.dispatch(FetchMovieListIfNotLoadedAction()),
      converter: (store) => MovieListPageViewModel.fromStore(store),
      builder: (_, viewModel) => MovieListPageContent(viewModel),
    );
  }
}

class MovieListPageContent extends StatelessWidget {
  MovieListPageContent(this.viewModel);
  final MovieListPageViewModel viewModel;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 80.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            child: LoadingView(
              status: viewModel.status,
              loadingContent: const MoviePlaceholder(),
              errorContent: const MoviePlaceholder(),
              successContent: MovieListView(viewModel.status, viewModel.movieLists),
            ),
          ),
        ],
      ),
    );
  }
}