import 'package:flutter/material.dart';
import 'package:flutter_list_movie/global/colors.dart';

class MainAppBar extends StatefulWidget {
  @override
  _MainAppBarState createState() => _MainAppBarState();
}

class _MainAppBarState extends State<MainAppBar> with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      centerTitle: false,
      leading: null,
      actions: <Widget>[
        new Padding(
          padding: EdgeInsets.only(left: 25.0, right: 20.0),
          child: new Text("List",
            style: TextStyle(
                color: colorGrayscale10,
                fontWeight: FontWeight.bold,
                fontSize: 44.0,
                letterSpacing: 2.0,
                fontFamily: 'Kenyc'
            ),
          ),
        ),
        new Flexible(
          child: TextField(
            // controller: _searchQuery,
            autofocus: false,
            decoration: InputDecoration(
              suffixIcon: new Icon(Icons.search, color: colorGrayscale10),
              hintText: "Find movies..",
              border: OutlineInputBorder(
                  gapPadding: 0.0,
                  borderSide: new BorderSide(color: colorGrayscale10),
                  borderRadius: BorderRadius.all(Radius.circular(15.0))
              ),
              hintStyle: TextStyle(color: colorGrayscale10),
              focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.0)),
                  borderSide: BorderSide(color: Colors.blue)),
              contentPadding: EdgeInsets.only(bottom: 20.0, left: 10.0),
            ),
            style: TextStyle(color: colorGrayscale10, fontSize: 16.0,),
            // onChanged: _updateSearchQuery,
          ),
        ),

      ],
      backgroundColor: colorMinionYellow,
    );
  }
}