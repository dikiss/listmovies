import 'package:flutter/material.dart';
import 'package:flutter_list_movie/pages/screens/splash_screen.dart';
import 'package:flutter_list_movie/redux/app/app_state.dart';
import 'package:flutter_list_movie/redux/movies/movies_list_actions.dart';
import 'package:flutter_list_movie/redux/store.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';

import 'global/colors.dart';

Future<void> main() async {
  final store = createStore();

  runApp(IMDbApp(store));
}

class IMDbApp extends StatefulWidget {
  IMDbApp(this.store);
  final Store<AppState> store;

  @override
  _IMDbAppState createState() => _IMDbAppState();
}

class _IMDbAppState extends State<IMDbApp> {
  @override
  void initState() {
    super.initState();
    widget.store.dispatch(InitAction());
  }

  @override
  Widget build(BuildContext context) {
    return StoreProvider<AppState>(
        store: widget.store,
        child: MaterialApp(
            title: 'List Movie',
            debugShowCheckedModeBanner: true,
            theme: ThemeData(primaryColor: colorMinionYellow, accentColor: colorWhite),
            home: SplashScreen()
        )
    );
  }
}