import 'package:flutter_list_movie/services/handler/movie_list_handler.dart';
import 'package:redux/redux.dart';

import 'app/app_reducer.dart';
import 'app/app_state.dart';
import 'movies/movie_list_middleware.dart';

Store<AppState> createStore() {
  final movieListAPI = MovieListHandler();

  return Store(
    appReducer,
    initialState: AppState.initial(),
    distinct: true,
    middleware: [
      MovieListMiddleware(movieListAPI)
    ],
  );
}