import 'package:flutter_list_movie/models/loading.dart';

import 'movie_list_state.dart';
import 'movies_list_actions.dart';

MovieListState movieListReducer(MovieListState state, dynamic action) {
  if (action is FetchMovieListAction) {
    return state.copyWith(loadingStatus: LoadingStatus.loading);
  } else if (action is GetMovieListSuccessAction) {
    final newMovieLists = action.movieLists.toList();
    return state.copyWith(
      loadingStatus: LoadingStatus.success,
      movieLists: newMovieLists,
    );
  } else if (action is GetMovieListErrorAction) {
    return state.copyWith(loadingStatus: LoadingStatus.error);
  }

  return state;
}