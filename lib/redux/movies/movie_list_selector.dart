import 'package:flutter_list_movie/models/movie_list.dart';
import 'package:flutter_list_movie/redux/app/app_state.dart';
import 'package:reselect/reselect.dart';

final movieListsSelector = createSelector1<AppState, List<MovieList>, List<MovieList>>(
      (state) => state.movieListState.movieLists,
      (List<MovieList> movieLists) {
    return movieLists;
  },
);