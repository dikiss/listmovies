import 'package:flutter/material.dart';
import 'package:flutter_list_movie/models/loading.dart';
import 'package:flutter_list_movie/models/movie_list.dart';

@immutable
class MovieListState {
  MovieListState({
    @required this.loadingStatus,
    @required this.movieLists,
  });

  final LoadingStatus loadingStatus;
  final List<MovieList> movieLists;

  factory MovieListState.initial() {
    return MovieListState(
      loadingStatus: LoadingStatus.idle,
      movieLists: [],
    );
  }

  MovieListState copyWith(
      { LoadingStatus loadingStatus, List<MovieList> movieLists }) {
    return MovieListState(
      loadingStatus: loadingStatus ?? this.loadingStatus,
      movieLists: movieLists ?? this.movieLists,
    );
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is MovieListState &&
              runtimeType == other.runtimeType &&
              loadingStatus == other.loadingStatus;

  @override
  int get hashCode => loadingStatus.hashCode;
}