import 'package:flutter_list_movie/redux/movies/movie_list_reducer.dart';

import 'app_state.dart';

AppState appReducer(AppState state, dynamic action) {
  return new AppState(
      movieListState: movieListReducer(state.movieListState, action)
  );
}